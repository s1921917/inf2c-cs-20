#=========================================================================
# XOR Cipher Encryption
#=========================================================================
# Encrypts a given text with a given key.
# 
# Inf2C Computer Systems
# 
# Dmitrii Ustiugov
# 9 Oct 2020
# 
#
#=========================================================================
# DATA SEGMENT
#=========================================================================
.data
#-------------------------------------------------------------------------
# Constant strings
#-------------------------------------------------------------------------

input_text_file_name:         .asciiz  "input_xor.txt"
key_file_name:                .asciiz  "key_xor.txt"
newline:                      .asciiz  "\n"
        
#-------------------------------------------------------------------------
# Global variables in memory
#-------------------------------------------------------------------------
# 
input_text:                   .space 10001       # Maximum size of input_text_file + NULL
.align 4                                         # The next field will be aligned
key:                          .space 33          # Maximum size of key_file + NULL
.align 4                                         # The next field will be aligned
key_bytes:                    .space 4           # Key is at most 4 bytes long

# You can add your data here!

#=========================================================================
# TEXT SEGMENT  
#=========================================================================
.text

#-------------------------------------------------------------------------
# MAIN code block
#-------------------------------------------------------------------------

.globl main                     # Declare main label to be globally visible.
                                # Needed for correct operation with MARS
main:
#-------------------------------------------------------------------------
# Reading file block. DO NOT MODIFY THIS BLOCK
#-------------------------------------------------------------------------

# opening file for reading (text)

        li   $v0, 13                    # system call for open file
        la   $a0, input_text_file_name  # input_text file name
        li   $a1, 0                     # flag for reading
        li   $a2, 0                     # mode is ignored
        syscall                         # open a file
        
        move $s0, $v0                   # save the file descriptor 

        # reading from file just opened

        move $t0, $0                    # idx = 0

READ_LOOP:                              # do {
        li   $v0, 14                    # system call for reading from file
        move $a0, $s0                   # file descriptor
                                        # input_text[idx] = c_input
        la   $a1, input_text($t0)             # address of buffer from which to read
        li   $a2,  1                    # read 1 char
        syscall                         # c_input = fgetc(input_text_file);
        blez $v0, END_LOOP              # if(feof(input_text_file)) { break }
        lb   $t1, input_text($t0)          
        beq  $t1, $0,  END_LOOP        # if(c_input == '\0')
        addi $t0, $t0, 1                # idx += 1
        j    READ_LOOP
END_LOOP:
        sb   $0,  input_text($t0)       # input_text[idx] = '\0'

        # Close the file 

        li   $v0, 16                    # system call for close file
        move $a0, $s0                   # file descriptor to close
        syscall                         # fclose(input_text_file)


# opening file for reading (key)

        li   $v0, 13                    # system call for open file
        la   $a0, key_file_name         # key file name
        li   $a1, 0                     # flag for reading
        li   $a2, 0                     # mode is ignored
        syscall                         # open a file
        
        move $s0, $v0                   # save the file descriptor 

        # reading from file just opened

        move $t0, $0                    # idx = 0

READ_LOOP1:                             # do {
        li   $v0, 14                    # system call for reading from file
        move $a0, $s0                   # file descriptor
                                        # key[idx] = c_input
        la   $a1, key($t0)              # address of buffer from which to read
        li   $a2,  1                    # read 1 char
        syscall                         # c_input = fgetc(key_file);
        blez $v0, END_LOOP1             # if(feof(key_file)) { break }
        lb   $t1, key($t0)          
        addi $v0, $0, 10                # newline \n
        beq  $t1, $v0, END_LOOP1        # if(c_input == '\n')
        addi $t0, $t0, 1                # idx += 1
        j    READ_LOOP1
END_LOOP1:
        sb   $0,  key($t0)              # key[idx] = '\0'

        # Close the file 

        li   $v0, 16                    # system call for close file
        move $a0, $s0                   # file descriptor to close
        syscall                         # fclose(key_file)

#------------------------------------------------------------------
# End of reading file block.
#------------------------------------------------------------------


# You can add your code here!
key_decode:
        .eqv k        $s0               # Key iterator
        .eqv key_len  $s1               # Key length (amount of bytes)
        .eqv k_bit    $s2               # "Bit" of key
        .eqv f        $s3               # File iterator
        .eqv c        $s4               # Char of file

        move k,       $0
        move key_len, $0
        li $t7, 7                       # Save 7 for sub
        
decode_loop:
        lb k_bit, key(k)
        beq k_bit, $0, encode           # EOF, finish loop

        .eqv byte_idx $t0
        .eqv bit_idx  $t1

        srl byte_idx, k, 3              # k `div` 3 gives byte position of key
        andi bit_idx, k, 7              # k `mod` 8 (power of two allows for k & (n - 1) to be used)
        sub bit_idx, $t7, bit_idx       # 7 - (k `mod` 8) gives bit position in byte of key

        addi k_bit, k_bit, -48          # Subtract the 0 character to give integer 0 or 1 from '0' or '1'
        sllv k_bit, k_bit, bit_idx      # Shift bit into position

        lb $t2, key_bytes(byte_idx)     # Load byte from key
        or $t2, $t2, k_bit              # Add bit to byte
        sb $t2, key_bytes(byte_idx)     # Save byte with bit added
        addiu k, k, 1                   # Increment k
        j decode_loop

encode:
        srl key_len, k, 3               # k `div` 3 will give the total key length
        move f, $0
        move k, $0                      # Reset k to iterate over bytes of the decoded key

        .eqv nl $t6                     # Save newline and space chars for comparison
        .eqv sp $t7

        li  nl, '\n'
        li  sp, ' '
        li $v0, 11                      # Print character syscall

encode_loop:
        lb c, input_text(f)             # Load byte from file
        beq c, $0, main_end             # EOF, end

        beq c, nl, print                # If char is a space or newline, just print it without encoding it
        beq c, sp, print

        encode_char:
                lb $t0, key_bytes(k)    # Load byte of key into $t0
                xor c, c, $t0           # XOR char
        print:
                move $a0, c             # Move c into $a0 for printing
                syscall                 # Print char
        
        addiu k, k, 1                   # Increment k
        addiu f, f, 1                   # Increment f
        bne k, key_len, encode_loop     # Key position is less than key length
        move k, $0                      # Otherwise reset it
        j encode_loop

#------------------------------------------------------------------
# Exit, DO NOT MODIFY THIS BLOCK
#------------------------------------------------------------------
main_end:      
        li   $v0, 10          # exit()
        syscall

#----------------------------------------------------------------
# END OF CODE
#----------------------------------------------------------------
