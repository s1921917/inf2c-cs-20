#include <stdio.h>

char* file = "ab cd\ne fg\n";
//           00001000 | 00010000
char* key = "0000100000010000";
unsigned char key_bytes[4];
unsigned int key_length = 0;

int main() {
    unsigned int k = 0; // Key iterator
    while (key[k] != '\0') {
        unsigned char kb = key[k]; // "Bit" of key
        unsigned int byte_index = k >> (unsigned) 3; // k `div` 3 gives byte of key
        unsigned int bit_index = 7 - (k & (unsigned) 7); // 7 - (k `mod` 8) gives bit position of byte
        unsigned char bit = kb - '0';

        bit = bit << bit_index; // Shift the bit into the position
        key_bytes[byte_index] |= bit; // Add bit to key
        k++; // Next bit of key
    }

    key_length = k >> (unsigned) 3; // Key length will be the iterator value `div` 3

    int i = 0; // File iterator
    k = 0; // Still the key iterator

    // Are we at the end of the file?
    while (file[i] != '\0') {
        unsigned char c = file[i];

        // Is the character a space or newline?
        if (c == ' ' || c == '\n') {
            printf("%c", c);
        } else {
            // XOR char with key
            unsigned char xord = c ^ key_bytes[k];
            printf("%c", xord);
        }

        k++; // Increment k

        // If k is now longer than the key, reset
        if (k == key_length) {
            k = 0;
        }

        i++; // Next char in file
    }

    return 0;
}
