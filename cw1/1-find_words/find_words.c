#include <stdlib.h>
#include <stdio.h>

char* file = "this is\nmy plain text\ni will not encrypt it"; // Input data for testing
char* words[50]; // Array of pointers to unique words
char word[20]; // Store working word

// Assembly be like
// "write ur own memcpy lol"
void s_memcpy(char* source, char* dest, unsigned int bytes) {
    for (int i = 0; i < bytes; ++i) {
        dest[i] = source[i];
    }
}

int main() {
    int i = 0; // File char counter
    int w = 0; // Word array index 
    int ws = 0; // Overall words list index 
    int word_length = 0;

    while (file[i] != '\0') {
        char c = file[i]; // Char from file

        // Are we at the end of a word?
        if (c == ' ' || c == '\n') {
            char* word_ptr = malloc(sizeof *word_ptr * word_length); // Allocate for word

            // Copy word into heap 
            s_memcpy(word, word_ptr, word_length);

            words[ws] = word_ptr; // Words now has a pointer to the new word, increment ws
            ++ws;
            w = 0; // Word index goes back to 0 as well as word length
            word_length = 0;

        } else {
            // Add the char to the current word, increment the word index and length
            word[w] = c;
            ++w;
            ++word_length;
        }

        ++i; // Increment file index
    }

    words[ws] = NULL; // End of array marker

    i = 0; // Counter for words array

    // Print all words with a newline
    while (words[i] != NULL) {
       printf("%s", words[i]);
       printf("\n");
       ++i; 
    }

    return 0;
}