#=========================================================================
# Word Finder 
#=========================================================================
# Finds words in a given text.
# 
# Inf2C Computer Systems
# 
# Dmitrii Ustiugov
# 9 Oct 2020
# 
#
#=========================================================================
# DATA SEGMENT
#=========================================================================
.data
#-------------------------------------------------------------------------
# Constant strings
#-------------------------------------------------------------------------

input_text_file_name:         .asciiz  "input_words.txt"
newline_char:                 .asciiz  "\n"
        
#-------------------------------------------------------------------------
# Global variables in memory
#-------------------------------------------------------------------------
# 
input_text:                   .space 10001       # Maximum size of input_text_file + NULL
.align 4                                         # The next field will be aligned
words:                        .space 3200        # Reserve an array of 50 pointers
current_word:                 .space 64          # Current working word, max 63 char long + NULL

# You can add your data here!

#=========================================================================
# TEXT SEGMENT  
#=========================================================================
.text

#-------------------------------------------------------------------------
# MAIN code block
#-------------------------------------------------------------------------

.globl main                     # Declare main label to be globally visible.
                                # Needed for correct operation with MARS
main:
#-------------------------------------------------------------------------
# Reading file block. DO NOT MODIFY THIS BLOCK
#-------------------------------------------------------------------------

# opening file for reading

        li   $v0, 13                    # system call for open file
        la   $a0, input_text_file_name  # input_text file name
        li   $a1, 0                     # flag for reading
        li   $a2, 0                     # mode is ignored
        syscall                         # open a file
        
        move $s0, $v0                   # save the file descriptor 

        # reading from file just opened

        move $t0, $0                    # idx = 0

READ_LOOP:                              # do {
        li   $v0, 14                    # system call for reading from file
        move $a0, $s0                   # file descriptor
                                        # input_text[idx] = c_input
        la   $a1, input_text($t0)             # address of buffer from which to read
        li   $a2,  1                    # read 1 char
        syscall                         # c_input = fgetc(input_text_file);
        blez $v0, END_LOOP              # if(feof(input_text_file)) { break }
        lb   $t1, input_text($t0)          
        beq  $t1, $0,  END_LOOP        # if(c_input == '\0')
        addi $t0, $t0, 1                # idx += 1
        j    READ_LOOP
END_LOOP:
        sb   $0,  input_text($t0)       # input_text[idx] = '\0'

        # Close the file 

        li   $v0, 16                    # system call for close file
        move $a0, $s0                   # file descriptor to close
        syscall                         # fclose(input_text_file)


#------------------------------------------------------------------
# End of reading file block.
#------------------------------------------------------------------

# You can add your code here!
counters_init:
        # Set up DEFINES for readability
        .eqv file_idx  $t0
        .eqv cword_idx $t1
        .eqv words_idx $t2
        .eqv word_len  $a0
        .eqv c         $t4
        .eqv one       $t5
        .eqv newline   $t6
        .eqv space     $t7

        move file_idx,  $0              # File current char index
        move cword_idx, $0              # Working word array index
        move words_idx, $0              # Words pointer array index
        li word_len, 1                  # Length of current word (will always have NULL)
        li one, 1                       # Load a register with 1 for subtraction
        li newline, 10                  # Load newline character for comparison
        li space, 32                    # Load space character for comparison

# Splits the input string into a list of pointers to words
split_loop:
        lb c, input_text(file_idx)      # Load byte of text into $t4 
        beq c, $0, print_init           # while (c != '\0')
        beq c, newline, store_word      # if (c == '\n' || c == ' ')
        beq c, space, store_word 
        j add_to_current_word

store_word:
        li $v0, 9                       # sbrk, $a0 will already contain amount to allocate
        syscall                         # Allocate heap to store word 
        sub word_len, word_len, one     # Subtract null byte from word length (current word won't end with a NULL)
        move $v1, $0                    # Counter for memcpy
        memcpy:
                lb $a1, current_word($v1) # Get char at index
                sb $a1, ($v0)           # Save char
                addi $v1, $v1, 1        # Increment memcpy address 
                add $v0, $v0, 1         # Increment save address
                bne word_len, $v1, memcpy # Loop back to copy all data

        addi $v0, $v0, 1                # Offset by one more to store NULL byte
        sb $0, ($v0)                    # Add NULL to the end of the word
        sub $v0, $v0, word_len          # Restore the original base address of word
        sub $v0, $v0, one
        sw $v0, words(words_idx)        # Save pointer in words array

        addi words_idx, words_idx, 4    # Increment words array index
        move cword_idx, $0              # Current word index now needs to reset
        li word_len, 1                  # And so does word length
        j split_continue                # Skip this character

add_to_current_word:
        sb c, current_word(cword_idx)   # Store current byte into working word
        addi cword_idx, cword_idx, 1    # Incrememnt working word index  
        addi word_len, word_len, 1      # Increment word length 

split_continue:
        addi file_idx, file_idx, 1      # Increment file char index
        j split_loop                    # Jump back

print_init:
        li $t0, 0                       # Index for words array
        li $v0, 4                       # Load print string syscall

print_loop:
        lw $a0, words($t0)              # Load address for word
        blez $a0, main_end              # If address is NULL, exit

        syscall                         # Print word at index
        la $a0, newline_char            # Load newline char in
        syscall                         # Print newline

        addi $t0, $t0, 4                # Increment array index
        j print_loop

#------------------------------------------------------------------
# Exit, DO NOT MODIFY THIS BLOCK
#------------------------------------------------------------------
main_end:      
        li   $v0, 10          # exit()
        syscall

#----------------------------------------------------------------
# END OF CODE
#----------------------------------------------------------------
