#=========================================================================
# XOR Cipher Cracking
#=========================================================================
# Finds the secret key for a given encrypted text with a given hint.
# 
# Inf2C Computer Systems
# 
# Dmitrii Ustiugov
# 9 Oct 2020
# 
#
#=========================================================================
# DATA SEGMENT
#=========================================================================
.data
#-------------------------------------------------------------------------
# Constant strings
#-------------------------------------------------------------------------

input_text_file_name:         .asciiz  "input_xor_crack.txt"
hint_file_name:               .asciiz  "hint.txt"
newline:                      .asciiz  "\n"
        
#-------------------------------------------------------------------------
# Global variables in memory
#-------------------------------------------------------------------------
# 
text_start:                   .space 1           # Space before text to line up with hint (no substrings)
input_text:                   .space 10001       # Maximum size of input_text_file + NULL
.align 4                                         # The next field will be aligned
hint:                         .space 101         # Maximum size of key_file + NULL
.align 4                                         # The next field will be aligned
hint_cleaned:                 .space 101         # The hint with newlines converted to spaces

# You can add your data here!

#=========================================================================
# TEXT SEGMENT  
#=========================================================================
.text

#-------------------------------------------------------------------------
# MAIN code block
#-------------------------------------------------------------------------

.globl main                     # Declare main label to be globally visible.
                                # Needed for correct operation with MARS
main:
#-------------------------------------------------------------------------
# Reading file block. DO NOT MODIFY THIS BLOCK
#-------------------------------------------------------------------------

# opening file for reading (text)

        li   $v0, 13                    # system call for open file
        la   $a0, input_text_file_name  # input_text file name
        li   $a1, 0                     # flag for reading
        li   $a2, 0                     # mode is ignored
        syscall                         # open a file
        
        move $s0, $v0                   # save the file descriptor 

        # reading from file just opened

        move $t0, $0                    # idx = 0

READ_LOOP:                              # do {
        li   $v0, 14                    # system call for reading from file
        move $a0, $s0                   # file descriptor
                                        # input_text[idx] = c_input
        la   $a1, input_text($t0)             # address of buffer from which to read
        li   $a2,  1                    # read 1 char
        syscall                         # c_input = fgetc(input_text_file);
        blez $v0, END_LOOP              # if(feof(input_text_file)) { break }
        lb   $t1, input_text($t0)          
        beq  $t1, $0,  END_LOOP        # if(c_input == '\0')
        addi $t0, $t0, 1                # idx += 1
        j    READ_LOOP
END_LOOP:
        sb   $0,  input_text($t0)       # input_text[idx] = '\0'

        # Close the file 

        li   $v0, 16                    # system call for close file
        move $a0, $s0                   # file descriptor to close
        syscall                         # fclose(input_text_file)


# opening file for reading (hint)

        li   $v0, 13                    # system call for open file
        la   $a0, hint_file_name        # hint file name
        li   $a1, 0                     # flag for reading
        li   $a2, 0                     # mode is ignored
        syscall                         # open a file
        
        move $s0, $v0                   # save the file descriptor 

        # reading from file just opened

        move $t0, $0                    # idx = 0

READ_LOOP1:                             # do {
        li   $v0, 14                    # system call for reading from file
        move $a0, $s0                   # file descriptor
                                        # hint[idx] = c_input
        la   $a1, hint($t0)             # address of buffer from which to read
        li   $a2,  1                    # read 1 char
        syscall                         # c_input = fgetc(key_file);
        blez $v0, END_LOOP1             # if(feof(key_file)) { break }
        lb   $t1, hint($t0)          
        addi $v0, $0, 10                # newline \n
        beq  $t1, $v0, END_LOOP1        # if(c_input == '\n')
        addi $t0, $t0, 1                # idx += 1
        j    READ_LOOP1
END_LOOP1:
        sb   $0,  hint($t0)             # hint[idx] = '\0'

        # Close the file 

        li   $v0, 16                    # system call for close file
        move $a0, $s0                   # file descriptor to close
        syscall                         # fclose(key_file)

#------------------------------------------------------------------
# End of reading file block.
#------------------------------------------------------------------


# You can add your code here!
init:
        .eqv key $s0                    # Current key to test
        .eqv f   $s1                    # File iterator
        .eqv hi  $s2                    # Hint iterator (if == len then key is correct)
        .eqv c   $s3                    # Current char
        .eqv h   $s4                    # Char of hint
        .eqv len $s5                    # Length of hint
        .eqv max $t5                    # Max size of key
        .eqv nl  $t6                    # Newline char
        .eqv sp  $t7                    # Space char

        move f, $0
        move key, $0
        li hi, 1                        # Start saving hint at 1 to add a space
        li max, 256
        li nl, '\n'
        li sp, ' '

        sb sp, hint_cleaned($0)         # Save a space to the start of hint cleaned (no substrings)
        sb sp, text_start($0)           # Save a space to start of text as well to match    

# Didn't realise hint can't be multiline
# Oh well, this code works anyway
clean_hint:
        lb h, hint(f)                   # Load char of hint
        beq h, $0, decrypt_init         # End of hint

        beq h, nl, hint_newline         # If h is a newline, save a space instead
        j save_hint_char

        hint_newline:
                sb sp, hint_cleaned(hi) # Save newline as a space
                j clean_continue
        
        save_hint_char:
                sb h, hint_cleaned(hi)   # Save char to new hint
        
clean_continue:
        addiu f, f, 1                   # Next char
        addiu hi, hi, 1                 # Next char in cleaned
        j clean_hint

decrypt_init:
	addiu f, f, 1
        sb sp, hint_cleaned(f)          # Save space to end of hint
        addiu hi, hi, 1                 
        move len, hi                    # hi now has length of hint
        move f, $0                      # Reset f
        move hi, $0			# Reset hi

test_loop:
        beq key, max, print_failure     # If key is == 256, no valid key was found

decrypt_loop:
        lb c, text_start(f)
        beq c, $0, test_continue        # EOF, test next key

        beq c, nl, clean_c_space        # Newline or space mean don't encrypt and save as space
        beq c, sp, clean_c_space

        xor c, c, key                   # Otherwise, xor with key
        j test_char

        clean_c_space:
                li c, ' '
                j test_char
        
        test_char:
                lb h, hint_cleaned(hi)  # Load char of hint to test at hint index
                beq c, h, valid_char    # If char is equal to hint char, it's valid

                move hi, $0             # Not valid, reset hint index
                j decrypt_continue
        
        valid_char:
                addiu hi, hi, 1         # Move to next char of hint
                beq hi, len, print_success # If hint index == len, it's a valid key! Print it

decrypt_continue:
        addiu f, f, 1                   # Next char of file
        j decrypt_loop

test_continue:
        move f, $0                      # Not a valid key, reset and try next one
        move hi, $0			# Reset hi in case never reset
        addiu key, key, 1
        j test_loop

print_failure:
        li $v0, 1                       # No key found, print -1
        li $a0, -1
        syscall
        j end

print_success:
        move $t0, $0                    # Key found, print binary representation. $t0 is the counter for the loop
        li $t1, 8                       # End when $t0 equals 8
        li $t2, 7                       # Save 7 for sub
        
print_loop:
        beq $t0, $t1, end               # $t0 == 8, end
        sub $t3, $t2, $t0               # Amount to bit shift by

        srlv $t4, key, $t3              # Shift key by shift amount
        andi $t4, $t4, 1                # and 1 to get a single bit

        li $v0, 1                       
        move $a0, $t4                   # Print bit
        syscall
        addiu $t0, $t0, 1
      	j print_loop

end:
        li $v0, 4
        la $a0, newline
        syscall
#------------------------------------------------------------------
# Exit, DO NOT MODIFY THIS BLOCK
#------------------------------------------------------------------
main_end:      
        li   $v0, 10          # exit()
        syscall

#----------------------------------------------------------------
# END OF CODE
#----------------------------------------------------------------
