//
// Created by fluxe on 22/10/2020.
//

// OLD, DOESN'T SUPPORT AVOIDING SUBSTRINGS

#include <stdio.h>
#include <stdint.h>

char* file = "ij kl\nm no\n";
char* hint = "cd e\n";
char hint_converted[50];
size_t key = 0;

int main() {
    size_t f = 0; // File iterator
    char c = 0; // Current char
    char h = 0; // Char of hint
    size_t len = 0;  // Length of hint
    size_t hi = 0; // Hint iterator (if == len then key is correct)

    // Replace all new lines in hint with spaces for consistency
    while (hint[f] != '\0') {
        c = hint[f]; // Char of hint
        if (c == '\n') {
            hint_converted[f] = ' ';
        } else {
            hint_converted[f] = c;
        }

        ++f;
    }

    len = f;
    f = 0; // Reset f

    while (key < 256) {
        // Decrypt character
        while (file[f] != '\0') {
            c = file[f];
            if (c == ' ' || c == '\n') {
                c = ' ';
            } else {
                c = c ^ key;
            }

            h = hint_converted[hi];
            if (c == h) {
                ++hi; // Correct char, move one forward in hint
                // End of hint, key is correct
                if (hi == len) {
                    // Dread it
                    // Run from it
                    // Goto arrives all the same
                    goto print;
                }
            } else {
                hi = 0; // Rest hint index back to 0, incorrect char
            }

            ++f; // Next char of file
        }

        f = 0; // Reset f
        ++key;
    }

    print:
    if (key < 256) {
        size_t i = 0;
        while (i != 8) {
            size_t shift = 7 - i;
            uint8_t bit = key >> shift;
            bit &= 1u;
            printf("%d", bit);

            ++i;
        }
    } else {
        printf("-1");
    }

    printf("\n");

    return 0;
}