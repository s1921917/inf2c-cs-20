#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

typedef struct sentence {
    uint32_t length;
    char** words;
} sentence;

// Input data for testing
char* file = "the sun is shining\nthe cake is ready\nthe dog is barking\nparents play with a little child\nand honest people never lie\nso far so good\nstay healthy sleep walk exercise and run regularly\n"; 
sentence* sentences[20]; // Array of pointers to unique sentences
char* current_words[20];  // Current words of sentence
char word[20]; // Current word

// Assembly be like
// "write ur own memcpy lol"
void s_memcpy(void* source, void* dest, unsigned int bytes) {
    char* c_source = (char*) source;
    char* c_dest = (char*) dest;

    for (int i = 0; i < bytes; ++i) {
        c_dest[i] = c_source[i];
    }
}

int main() {
    int i = 0; // File char counter
    int w = 0; // Word array index 
    int ws = 0; // Overall words list index 
    int s = 0;  // Sentence array index
    int word_length = 0;
    int sentence_length = 0;

    while (file[i] != '\0') {
        char c = file[i]; // Char from file

        // Are we at the end of a word or sentence?
        if (c == ' ' || c == '\n') {
            char* word_ptr = malloc(sizeof *word_ptr * word_length); // Allocate for word

            // Copy word into heap 
            s_memcpy(word, word_ptr, word_length);

            current_words[ws] = word_ptr; // Words now has a pointer to the new word, increment ws
            ++ws;
            w = 0; // Word index goes back to 0 as well as word length
            word_length = 0;
            ++sentence_length; // Sentence length goes up by one
            // Are we at the end of a sentence?
            if (c == '\n') {
                sentence* sentence_ptr = malloc(sizeof *sentence_ptr); // Allocate a sentence
                char** words_ptr = malloc(sizeof *words_ptr * sentence_length); // Allocate word array

                // Copy words pointers into heap
                s_memcpy(current_words, words_ptr, sizeof *words_ptr * sentence_length);

                sentence_ptr->length = sentence_length;
                sentence_ptr->words = words_ptr; 
                sentences[s] = sentence_ptr;
                ++s;
                ws = 0;
                sentence_length = 0;
            }
        } else {
            // Add the char to the current word, increment the word index and length
            word[w] = c;
            ++w;
            ++word_length;
        }

        ++i; // Increment file index
    }

    sentences[s] = NULL; // End of array marker

    i = 0; // Counter for sentences array
    int line_no = 0; // Counter for line_no
    int new_line = 1; // Is new line?

    while (sentences[i] != NULL) {
        sentence* current_sentence = sentences[i];
        if (current_sentence->length >= line_no) {
            if (!new_line) {
                printf(" ");
            } else {
                new_line = 0;
            }
            
            printf("%s", current_sentence->words[i]);
        } else {
            printf("\n");
            new_line = 1;
        }
        ++line_no;
        ++i;
    }
    printf("\n");

    return 0;
}