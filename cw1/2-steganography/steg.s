#=========================================================================
# Steganography
#=========================================================================
# Retrive a secret message from a given text.
# 
# Inf2C Computer Systems
# 
# Dmitrii Ustiugov
# 9 Oct 2020
# 
#
#=========================================================================
# DATA SEGMENT
#=========================================================================
.data
#-------------------------------------------------------------------------
# Constant strings
#-------------------------------------------------------------------------

input_text_file_name:         .asciiz  "input_steg.txt"
newline:                      .asciiz  "\n"
space:                        .asciiz  " "
        
#-------------------------------------------------------------------------
# Global variables in memory
#-------------------------------------------------------------------------
# 
input_text:                   .space 10001       # Maximum size of input_text_file + NULL
.align 4                                         # The next field will be aligned
sentences:                    .space 1280        # Reserve space for 20 sentence structs
words:                        .space 640         # Space for current words in a sentence
current_word:                 .space 20          # Current word chars

# You can add your data here!

#=========================================================================
# TEXT SEGMENT  
#=========================================================================
.text

# Assembly be like
# "write ur own memcpy lol"
# $a0: source, $a1: dest, $a2 bytes to copy
# Modifies $a0, $a1, $t0, and $t1
memcpy:
        li $t0, 0               # Use t0 as index
        _memcpy_loop:
                lb $t1, ($a0)           # Load byte from source into $t1
                sb $t1, ($a1)           # Save byte from $t1 into dest
                addiu $a0, $a0, 1       # Increment source
                addiu $a1, $a1, 1       # Increment dest
                addiu $t0, $t0, 1       # Increment index
                bne $t0, $a2, _memcpy_loop # Loop back to copy all data

        jr $ra                  # Nothing more to do, return 

#-------------------------------------------------------------------------
# MAIN code block
#-------------------------------------------------------------------------

.globl main                     # Declare main label to be globally visible.
                                # Needed for correct operation with MARS
main:
#-------------------------------------------------------------------------
# Reading file block. DO NOT MODIFY THIS BLOCK
#-------------------------------------------------------------------------

# opening file for reading

        li   $v0, 13                    # system call for open file
        la   $a0, input_text_file_name  # input_text file name
        li   $a1, 0                     # flag for reading
        li   $a2, 0                     # mode is ignored
        syscall                         # open a file
        
        move $s0, $v0                   # save the file descriptor 

        # reading from file just opened

        move $t0, $0                    # idx = 0

READ_LOOP:                              # do {
        li   $v0, 14                    # system call for reading from file
        move $a0, $s0                   # file descriptor
                                        # input_text[idx] = c_input
        la   $a1, input_text($t0)             # address of buffer from which to read
        li   $a2,  1                    # read 1 char
        syscall                         # c_input = fgetc(input_text_file);
        blez $v0, END_LOOP              # if(feof(input_text_file)) { break }
        lb   $t1, input_text($t0)          
        beq  $t1, $0,  END_LOOP        # if(c_input == '\0')
        addi $t0, $t0, 1                # idx += 1
        j    READ_LOOP
END_LOOP:
        sb   $0,  input_text($t0)       # input_text[idx] = '\0'

        # Close the file 

        li   $v0, 16                    # system call for close file
        move $a0, $s0                   # file descriptor to close
        syscall                         # fclose(input_text_file)


#------------------------------------------------------------------
# End of reading file block.
#------------------------------------------------------------------


# You can add your code here!
counters_init:
        .eqv file_idx  $s0
        .eqv cword_idx $s1
        .eqv words_idx $s2
        .eqv sen_idx   $s3
        .eqv word_len  $s4
        .eqv sen_len   $s5
        .eqv c         $s6
        .eqv nl        $t6
        .eqv sp        $t7

        move file_idx,  $0              # File current char index
        move cword_idx, $0              # Working word array index
        move words_idx, $0              # Working words pointer array index
        move sen_idx,   $0              # Sentence array index
        move sen_len,   $0              # Sentence length
        move word_len,  $0              # Word length
        li         nl,  10              # Newline char
        li         sp,  32              # Space char  

# Splits the input string into a list of structs,
# defined as { uint32_t length; char** words; }
# Words points to an array of pointers on the heap,
# which itself will point to individual words on the heap
# Probably the easiest way to do this one(tm)
split_loop:
        lb c, input_text(file_idx)      # Load byte of text into c
        beq c, $0, decode_init          # while (c != '\0')
        beq c, nl, save                 # if (c == '\n' || c == ' ')
        beq c, sp, save
        j add_to_current_word

# Saves words and sentences to the heap
save:
        li $v0, 9                       # sbrk, save word length to $a0
        move $a0, word_len              
        addiu $a0, $a0, 1               # Add one byte for \0
        syscall                         # $v0 will now have the address to heap memory

        la   $a0, current_word          # Copy word into heap
        move $a1, $v0
        move $a2, word_len              
        jal  memcpy      

        addiu $a1, $a1, 1               # $a1 is now a pointer to the last char of word, add 1
        sb $0, ($a1)                    # So we can save the null byte
        
        sw $v0, words(words_idx)        # Save pointer to word to current words
        addi words_idx, words_idx, 4    # Increment words array index
        move cword_idx, $0              # Reset current word char index
        move word_len, $0               # and word length
        addiu sen_len, sen_len, 1       # Sentence length goes up by one

        bne c, nl, split_continue       # Are we at the end of a sentence?

        li $v0, 9                       # sbrk one sentence struct
        li $a0, 8                       # struct is 8 bytes to remain word aligned
        syscall
        move $t2, $v0                   # Save address of struct

        li $v0, 9                       # sbrk a word array
        move $a0, sen_len
        sll $a0, $a0, 2                 # Size is sentence_length * 4, or len << 2
        move $t0, $a0                   # Save size to t0 for later
        syscall
        move $t3, $v0                   # Save address of word array 

        la   $a0, words                 # Copy word pointers into heap
        move $a1, $t3
        move $a2, $t0                   # $t0 had the sentence byte length saved earlier
        jal  memcpy                     

        sw sen_len,  ($t2)              # Save the length of the sentence to the new struct 
        sw     $t3, 4($t2)              # Save the address to the start of the word array to the new struct
        sw     $t2, sentences(sen_idx)  # Save the pointer to the struct to the overall sentences array

        addiu sen_idx, sen_idx, 4       # Increment sentence index
        move words_idx, $0              # Reset current words index
        move sen_len, $0                # Reset sentence length

        j split_continue                # Don't save this char to the current word

add_to_current_word:
        sb c, current_word(cword_idx)   # Save the current byte to the working word
        addiu cword_idx, cword_idx, 1   # Increment working word index
        addiu word_len, word_len, 1     # Increment word length

split_continue:
        addiu file_idx, file_idx, 1     # Increment file char index
        j split_loop                    # Loop back

decode_init:
        move $t0, $0                    # Counter for sentences array
        move $t1, $0                    # Counter for line number
        li   $t2,  1                    # Boolean, are we on a new line?
        li   $v0,  4                    # Save print string syscall

decode_loop:
        lw $s3, sentences($t0)          # Address of sentence struct
        beq $s3, $0, decode_end         # NULL == end of array

        lw $s0,  ($s3)                  # Length of sentence
        lw $s1, 4($s3)                  # Pointer to words pointer array

        slt $t3, $t1, $s0               # Is line number less than sentence length?
        beq $t3, $0, print_newline      # If it is, print a new line 

        bne $t2, $0, on_newline         # Have we just started a new line?
        la $a0, space                   # Not on the start of a new line, print a space before word
        syscall
        j print_word

        on_newline:
                move $t2, $0            # Not on the start of a new line after this word

        print_word:
                sll $t3, $t1, 2         # Word at line number is line_number * 4
                addu $s1, $s1, $t3      # Get address of word array at line number
                lw $a0, ($s1)           # Get address to word
                syscall
                j decode_continue

        print_newline:
                la $a0, newline
                syscall
                li $t2, 1               # Starting a new line
        
decode_continue:
        addiu $t1, $t1, 1               # Next line
        addiu $t0, $t0, 4               # Next struct
        j decode_loop
        

decode_end:
        la $a0, newline                 # Finish with a newline for old times' sake
        syscall

#------------------------------------------------------------------
# Exit, DO NOT MODIFY THIS BLOCK
#------------------------------------------------------------------
main_end:      
        li   $v0, 10          # exit()
        syscall

#----------------------------------------------------------------
# END OF CODE
#----------------------------------------------------------------
