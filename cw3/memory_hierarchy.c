/*************************************************************************************|
|   1. YOU ARE NOT ALLOWED TO SHARE/PUBLISH YOUR CODE (e.g., post on piazza or online)|
|   2. Fill memory_hierarchy.c                                                        |
|   3. Do not use any other .c files neither alter mipssim.h or parser.h              |
|   4. Do not include any other library files                                         |
|*************************************************************************************/

#include "mipssim.h"
#define BLOCK_SIZE 16
#define ADDRESS_SIZE 32

uint32_t cache_type = 0;

typedef struct cache_block {
    bool valid;
    uint32_t age;
    uint32_t tag;
    uint32_t data[BLOCK_SIZE / 4];
} CacheBlock;

typedef struct cache_addr_parts {
    uint32_t tag;
    uint32_t index;
    uint32_t offset;
} CacheAddrParts;

uint32_t offset_bits;
uint32_t sets;
uint32_t index_bits;
uint32_t tag_bits;
uint32_t age; // For associative cache
// bool cache_full; // Flag for fully associative cache
CacheBlock* cache;

void memory_state_init(struct architectural_state *arch_state_ptr) {
    arch_state_ptr->memory = (uint32_t *) malloc(sizeof(uint32_t) * MEMORY_WORD_NUM);
    memset(arch_state_ptr->memory, 0, sizeof(uint32_t) * MEMORY_WORD_NUM);
    if (cache_size == 0) {
        // CACHE DISABLED
        memory_stats_init(arch_state_ptr, 0); // WARNING: we initialize for no cache 0
    } else {
        // CACHE ENABLED

        // Always the same amount
        offset_bits = (uint32_t) log2(BLOCK_SIZE);

        switch(cache_type) {
        case CACHE_TYPE_DIRECT:; // direct mapped
            sets = cache_size / BLOCK_SIZE;
            index_bits = (uint32_t) log2(sets);
            tag_bits = ADDRESS_SIZE - offset_bits - index_bits;
            memory_stats_init(arch_state_ptr, tag_bits);
            cache = calloc(sets, sizeof *cache);
            break;
        case CACHE_TYPE_FULLY_ASSOC: // fully associative
            sets = cache_size / BLOCK_SIZE; // Technically not "sets"
            index_bits = 0; // No index in fully associated
            tag_bits = ADDRESS_SIZE - offset_bits;
            memory_stats_init(arch_state_ptr, tag_bits);
            cache = calloc(sets, sizeof *cache);
            break;
        case CACHE_TYPE_2_WAY: // 2-way associative
            sets = cache_size / BLOCK_SIZE / 2;
            index_bits = (uint32_t) log2(sets);
            tag_bits = ADDRESS_SIZE - index_bits - offset_bits;
            memory_stats_init(arch_state_ptr, tag_bits);
            // Allocate twice the number of sets, as each set has two entries
            cache = calloc(sets * 2, sizeof *cache);
            break;
        default:
            assert(0);
        }
    }
}

// Returns the offset, index and tag from an address
CacheAddrParts get_addr_parts(int address) {
    CacheAddrParts parts;

    parts.offset = get_piece_of_a_word(address, 0, offset_bits);
    parts.index = get_piece_of_a_word(address, offset_bits, index_bits);
    parts.tag = get_piece_of_a_word(address, offset_bits + index_bits, tag_bits);

    if (cache_type == CACHE_TYPE_2_WAY) {
        parts.index *= 2;
    }

    return parts;
}

// Returns true if there was a cache hit, false otherwise
// Optionally takes an index pointer to return the index of hit for associative
bool detect_cache_hit(CacheAddrParts addr_parts, uint32_t* index) {
    CacheBlock* entry;

    switch (cache_type) {
    case CACHE_TYPE_DIRECT:
        entry = &cache[addr_parts.index];
        if (entry->valid && entry->tag == addr_parts.tag) return true;

        break;
    case CACHE_TYPE_FULLY_ASSOC:
        for (int i = 0; i < sets; ++i) {
            entry = &cache[i];

            if (entry->valid && entry->tag == addr_parts.tag) {
                *index = i;
                return true;
            }
        }
        break;
    case CACHE_TYPE_2_WAY:
        for (int i = 0; i < 2; ++i) {
            entry = &cache[addr_parts.index + i];

            if (entry->valid && entry->tag == addr_parts.tag) {
                *index = i;
                return true;
            }
        }
        break;
    default:
        assert(0);
    }

    return false;
}

// Fills a new cache block from memory address at a given index
void fill_cache_block(uint32_t index, uint32_t address) {
    CacheAddrParts addr_parts = get_addr_parts(address);
    CacheBlock* entry = &cache[index];
    entry->valid = true;
    entry->tag = addr_parts.tag;
    for (int i = 0; i < BLOCK_SIZE / 4; ++i) {
        entry->data[i] = arch_state.memory[(address - addr_parts.offset) / 4 + i];
    }
}

// returns data on memory[address / 4]
int memory_read(int address){
    arch_state.mem_stats.lw_total++;
    check_address_is_word_aligned(address);

    if (cache_size == 0) {
        // CACHE DISABLED
        return (int) arch_state.memory[address / 4];
    } else {
        // CACHE ENABLED
        /// @students: your implementation must properly increment: arch_state_ptr->mem_stats.lw_cache_hits
        CacheAddrParts addr_parts = get_addr_parts(address);
        uint32_t index; // Index of hit for associative models
        uint32_t data; // Data to return

        switch(cache_type) {
        case CACHE_TYPE_DIRECT: // direct mapped
            if (detect_cache_hit(addr_parts, NULL)) {
                arch_state.mem_stats.lw_cache_hits++;
                data = cache[addr_parts.index].data[addr_parts.offset / 4];
            } else {
                fill_cache_block(addr_parts.index, address);
                data = arch_state.memory[address / 4];
            }
            break;
        case CACHE_TYPE_FULLY_ASSOC: // fully associative
            if (detect_cache_hit(addr_parts, &index)) {
                arch_state.mem_stats.lw_cache_hits++;
                data = cache[index].data[addr_parts.offset / 4];
            } else {
                // Allocation of new cache block
                // If the cache is not full, just find the next free block
                bool is_free_space = false;
                for (int i = 0; i < sets; ++i) {
                    if (!cache[i].valid) {
                        // Still free space in cache, return index of free block
                        is_free_space = true;
                        index = i;
                        break;
                    }
                }

                if (!is_free_space) {
                    // Find the least recently accessed block
                    int oldest_age = cache[0].age;
                    index = 0;
                    for (int i = 1; i < sets; ++i) {
                        if (cache[i].age < oldest_age) {
                            oldest_age = cache[i].age;
                            index = i;
                        }
                    }
                }

                // Fill the cache as usual
                fill_cache_block(index, address);
                data = arch_state.memory[address / 4];
            }

            // Update the age of the accessed block and increment age
            cache[index].age = age;
            age++;
            break;
        case CACHE_TYPE_2_WAY: // 2-way associative
            if (detect_cache_hit(addr_parts, &index)) {
                arch_state.mem_stats.lw_cache_hits++;
                data = cache[addr_parts.index + index].data[addr_parts.offset / 4];
            } else {
                // Allocation of new cache block
                bool is_free_space = false;
                for (int i = 0; i < 2; ++i) {
                    if (!cache[addr_parts.index + i].valid) {
                        is_free_space = true;
                        index = i;
                        break;
                    }
                }

                if (!is_free_space) {
                    int a_age = cache[addr_parts.index].age;
                    int b_age = cache[addr_parts.index + 1].age;

                    index = a_age < b_age ? 0 : 1;
                }

                // Fill the cache as usual
                fill_cache_block(addr_parts.index + index, address);
                data = arch_state.memory[address / 4];
            }

            cache[addr_parts.index + index].age = age;
            age++;
            break;
        default:
            assert(0);
        }

        return data;
    }
}

// writes data on memory[address / 4]
void memory_write(int address, int write_data) {
    arch_state.mem_stats.sw_total++;
    check_address_is_word_aligned(address);

    if (cache_size == 0) {
        // CACHE DISABLED
        arch_state.memory[address / 4] = (uint32_t) write_data;
    } else {
        // CACHE ENABLED
        /// @students: your implementation must properly increment: arch_state_ptr->mem_stats.sw_cache_hits
        CacheAddrParts addr_parts = get_addr_parts(address);
        uint32_t index; // Index of hit for associative models

        switch(cache_type) {
        case CACHE_TYPE_DIRECT: // direct mapped
            // No index needs to be searched for, pass a null pointer
            if (detect_cache_hit(addr_parts, NULL)) {
                // Hit, write entry to cache
                arch_state.mem_stats.sw_cache_hits++;
                CacheBlock* entry = &cache[addr_parts.index];
                entry->data[addr_parts.offset / 4] = write_data;
            }
            break;
        case CACHE_TYPE_FULLY_ASSOC:; // fully associative
            if (detect_cache_hit(addr_parts, &index)) {
                // Hit, write entry to cache and update age
                arch_state.mem_stats.sw_cache_hits++;
                CacheBlock* entry = &cache[index];
                entry->data[addr_parts.offset / 4] = write_data;
                entry->age = age;
                age++;
            }
            break;
        case CACHE_TYPE_2_WAY: // 2-way associative
            if (detect_cache_hit(addr_parts, &index)) {
                arch_state.mem_stats.sw_cache_hits++;
                CacheBlock* entry = &cache[addr_parts.index + index];
                entry->data[addr_parts.offset / 4] = write_data;
                entry->age = age;
                age++;
            }
            break;
        default:
            assert(0);
        }

        // Write data to memory
        arch_state.memory[address / 4] = write_data;
    }
}
