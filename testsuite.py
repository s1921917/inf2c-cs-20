import random, string, sys, os, os.path, subprocess

input_file_names = ["\\cw1\\1-find_words\\input_words.txt", "\\cw1\\2-steganography\\input_steg.txt", "\\cw1\\3-xor\\input_xor.txt", "\\cw1\\4-book\\book.txt", "\\cw1\\5-xor_cracking\\input_xor_crack.txt"]
extra_files      = ["\\cw1\\3-xor\\key_xor.txt", "\\cw1\\4-book\\input_book_cipher.txt", "\\cw1\\5-xor_cracking\\hint.txt"]
num_of_tests     = 100

d = os.getcwd()
input_file_names = [d + x for x in input_file_names]

print(input_file_names)

def sorn(p):
    return '\n' if random.random() < p else ' '

def run_tests(test_num):
    if int(test_num[0]) == 1:
        gen_and_run_task1(test_num)

    if int(test_num[0]) == 2:
        gen_and_run_task2(test_num)

    if int(test_num[0]) == 3:
        gen_and_run_task3(test_num)

    if int(test_num[0]) == 4:
        gen_and_run_task4(test_num)

    if int(test_num[0]) == 5:
        gen_and_run_task5(test_num)
    


def gen_and_run_task1(test_num):
    test_pass_counter = 0
    for i in range(num_of_tests):
        input_file = generate_input_file(test_num)
        input_file_joined = ''.join(input_file)
        input_file_joined = input_file_joined.split()
        output = '\n'.join(input_file_joined) + '\n'

        with open(d + "\\cw1\\1-find_words\\output.txt", "w+", newline='\n') as file:
            file.write(output)
 
        
        os.chdir(d + "\\cw1\\1-find_words\\")
        result = subprocess.run(["java", "-jar", "Mars4_5.jar", "sm", "me", "find_words.s"], stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)
        result = result.stdout
        result_text = result.decode("utf-8")
        if result_text == output:
            test_pass_counter += 1
        else:
            print("TEST FAILED")
            print("-----------")
            print("INPUT")
            print(''.join(input_file))
            print()
            print("EXPECTED OUTPUT")
            print(output)
            print("ACTUAL OUTPUT")
            print(result_text)
            break

    print("Task 1: Passed " + str(test_pass_counter) + "/" + str(num_of_tests) +  " tests")



def gen_and_run_task2(test_num):
    test_pass_counter = 0
    for i in range(num_of_tests):
        input_file = generate_input_file(test_num)
        input_file_joined = ''.join(input_file)
        input_file_joined = input_file_joined.split('\n')[:-1]
        input_handled = [x.split(' ') for x in input_file_joined]
        out_text = ""
        output = []

        for i in range(len(input_handled)):
            if i >= len(input_handled[i]):
                output.append('\n')
            else:
                output.append(input_handled[i][i])

        n = 0
        while n < len(output):
            if n+1 < len(output):
                if output[n+1] == '\n':
                    out_text += output[n]
                elif output[n] != '\n':
                    out_text += output[n]
                    out_text += ' '
                else:
                    out_text += output[n]
                n += 1
            else:
                out_text += output[n]
                n += 1

        out_text += '\n'

        with open(d + "\\cw1\\2-steganography\\output.txt", "w+", newline='\n') as file:
            file.write(out_text)
 
        
        os.chdir(d + "\\cw1\\2-steganography\\")
        result = subprocess.run(["java", "-jar", "Mars4_5.jar", "sm", "me", "steg.s"], stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)
        result = result.stdout
        if result.decode("utf-8") == out_text:
            test_pass_counter += 1

    print("Task 2: Passed " + str(test_pass_counter) + "/" + str(num_of_tests) +  " tests")
            

def gen_and_run_task3(test_num):
    test_pass_counter = 0
    for x in range(num_of_tests):
        #Generate key
        random_key = ''.join(random.choice(string.printable) for _ in range(int(random.randint(1,4))))
        binary_key = ''.join("{0:08b}".format(ord(x), 'b') for x in random_key)
        binary_key += ("\n")

        #print(binary_key)
        with open(d + extra_files[0], "w+", newline='\n') as file:
                file.write(binary_key)

        input_file = generate_input_file(test_num)
        input_file_joined = ''.join(input_file)

        output = ""

        for i in range(len(input_file_joined)):
            if input_file_joined[i] != ' ' and input_file_joined[i] != '\n':
                output += chr(ord(input_file_joined[i]) ^ ord(random_key[i % len(random_key)]))
            if input_file_joined[i] == ' ':
                output += ' '
            elif input_file_joined[i] == '\n':
                output += '\n'

        with open(d + "\\cw1\\3-xor\\output.txt", "w+", newline='\n') as file:
                file.write(output)

        
        os.chdir(d + "\\cw1\\3-xor\\")
        result = subprocess.run(["java", "-jar", "Mars4_5.jar", "sm", "me", "xor.s"], stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)
        result = result.stdout
        if result.decode("utf-8") == output:
            test_pass_counter += 1

    print("Task 3: Passed " + str(test_pass_counter) + "/" + str(num_of_tests) +  " tests")



def gen_and_run_task4(test_num):
    test_pass_counter = 0
    for x in range(num_of_tests):
        input_file = generate_input_file(test_num)
        input_file_joined = ''.join(input_file)
        number_of_lines = len([x for x in input_file if x == '\n']) # XD

        book_indexes = ""
        for i in range(random.randint(1, 3*number_of_lines)):
            random_line_num = str(random.randint(1, number_of_lines)) # Line num will never be invalid :)
            random_word_num = str(random.randint(1, 60)) # Hope for the best lol

            book_indexes += random_line_num
            book_indexes += " "
            book_indexes += random_word_num
            book_indexes += "\n"
        
        with open(d + extra_files[1], "w+", newline='\n') as file:
                file.write(book_indexes)

        book_indexes = book_indexes.split('\n')[:-1]

        book_indexes = [x.split(' ') for x in book_indexes]

        input_handled = input_file_joined.split('\n')[:-1]
        input_handled = [x.split(' ') for x in input_handled]
        output = []

        for n in range(len(book_indexes)):
            line_num = int(book_indexes[n][0]) - 1
            word_num = int(book_indexes[n][1]) - 1

            line = input_handled[line_num]

            if(word_num >= len(line)):
                output.append('\n')
            else:
                output.append(line[word_num])

        out_text = ""
        n = 0
        while n < len(output):
            if n+1 < len(output):
                if output[n+1] == '\n':
                    out_text += output[n]
                elif output[n] != '\n':
                    out_text += output[n]
                    out_text += ' '
                else:
                    out_text += output[n]
                n += 1
            else:
                out_text += output[n]
                n += 1

        out_text += '\n'

        with open(d + "\\cw1\\4-book\\output.txt", "w+", newline='\n') as file:
            file.write(out_text)

        
        os.chdir(d + "\\cw1\\4-book\\")
        result = subprocess.run(["java", "-jar", "Mars4_5.jar", "sm", "me", "book.s"], stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)
        result = result.stdout
        if result.decode("utf-8") == out_text:
            test_pass_counter += 1

    print("Task 4: Passed " + str(test_pass_counter) + "/" + str(num_of_tests) +  " tests")

def gen_and_run_task5(test_num):
     test_pass_counter = 0
     for x in range(num_of_tests):
        #Generate key
        random_key = 96
        while (random_key >= 96 and random_key <= 128) or (random_key >= 65 and random_key <= 90): # This is to avoid random newlines and spaces that will fuck the input up
            random_key = random.randint(0, 255)
        
        binary_key = f'{random_key:08b}'
        binary_key += "\n"

        #print(binary_key)
        with open(d + "\\cw1\\5-xor_cracking\\output.txt", "w+", newline='\n') as bkey:
                bkey.write(binary_key)

        input_file = generate_input_file(test_num)
        input_file_joined = ''.join(input_file)
        input_random = input_file_joined.replace('\n', ' ')

        output = bytearray(b'')

        for i in range(len(input_file_joined)):
            if input_file_joined[i] != ' ' and input_file_joined[i] != '\n':
                output.append(ord(input_file_joined[i]) ^ random_key)
            if input_file_joined[i] == ' ':
                output.append(0x20)
            elif input_file_joined[i] == '\n':
                output.append(0x0a)

        with open(d + "\\cw1\\5-xor_cracking\\input_xor_crack.txt", "wb+") as file:
                file.write(output)
        
        random_thing = sorn(0.1) #scuffed solution
        hint = ""

        if random_thing == '\n':
            binary_key = "-1\n"
            hint = "no fucking way" #no fucking way we'll randomly gen this 
        else:
            random_num_1 = random.randint(0, len(input_random))
            random_num_2 = random.randint(0, len(input_random))

            while abs(random_num_1 - random_num_2) <= 1:
                random_num_1 = random.randint(0, len(input_random))
                random_num_2 = random.randint(0, len(input_random))

            if random_num_1 < random_num_2:
                hint = input_random[random_num_1:random_num_2] #Choose a random word
            else:
                hint = input_random[random_num_2:random_num_1]
            
            if len(hint) > 100:
                hint = hint[0:99]

            if hint[len(hint) - 1] == ' ':
                hint = hint[:-1]
        
        hint += '\n'

        with open(d + "\\cw1\\5-xor_cracking\\hint.txt", "w+", newline='\n') as hint_file:
                hint_file.write(hint)

        os.chdir(d + "\\cw1\\5-xor_cracking\\")
        result = subprocess.run(["java", "-jar", "Mars4_5.jar", "sm", "me", "xor_crack.s"], stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)
        result = result.stdout

        if result.decode("utf-8") == binary_key:
            test_pass_counter += 1
        # else:
            # print(binary_key, end='\n\n')
            # print(bytearray(input_file_joined, "utf-8"), end='\n\n')
            # print(bytearray(hint, "utf-8"), end='\n\n')
            # print(output, end='\n\n')



     print("Task 5: Passed " + str(test_pass_counter) + "/" + str(num_of_tests) +  " tests")


def generate_input_file(test_num):
    input_file = input_file_names[int(test_num[0]) - 1]
    with open(input_file, "w+",newline='\n') as file:
        random_str = []
        for i in range(random.randint(1, 800)):
            random_str.append(''.join(random.choice(string.ascii_lowercase) for _ in range(int(random.choice(string.digits[1:])))))
            random_str.append(sorn(0.05))
            
        if random_str[len(random_str) - 1] == ' ':
            random_str[len(random_str) - 1] = '\n'
        elif random_str[len(random_str) - 2] == ' ' and random_str[len(random_str) - 1] == '\n':
            random_str = random_str[:-1]
            random_str[len(random_str) - 1] = '\n'

        file.write(''.join(random_str))

        return random_str

def main(argv):
    if len(argv) == 0:
        print("Error, specify which task you want to do: python TestSuite.py <tasknum>")
        sys.exit(2)

    if int(argv[0]) > 6:
        print("Task number doesn't exist")
        sys.exit(2)

    run_tests(argv)

if __name__ == "__main__":
   main(sys.argv[1:]) 